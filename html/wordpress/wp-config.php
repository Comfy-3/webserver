k<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wpress' );

/** Database username */
define( 'DB_USER', 'melina' );

/** Database password */
define( 'DB_PASSWORD', 'password' );

/** Database hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'p>-XblUCZs^z|+E6,U5;kkL6{a=qjx@j+}|]4U_9jm8nB54f,)>9.Gq28}0())T/' );
define( 'SECURE_AUTH_KEY',  'NEOcf8=M1Y[BO~Gv0e}Hf*>Qn@?dc&r:-*~sL396|.wb<[F49fp2d&HHGH2vZ_o.' );
define( 'LOGGED_IN_KEY',    '-uU`eX;;lP/h|[vYA{;Eqc4% kz8xd4ylO#M;0Y[_.40n/P@$0k5{xcL&f79}9Kg' );
define( 'NONCE_KEY',        'CT-QO)`]N(0HBATO,5}I5KSs  t(>=R~a] i0Tv,U)>9aq)s=*o}Bt+IR8Ftq<];' );
define( 'AUTH_SALT',        '9I@Fmg]sgbJw)Yc,~^>SYQcsmF39}l&m4+!tdD5V [5_02>^@ij1(/zO`l8x}MeD' );
define( 'SECURE_AUTH_SALT', 'Usn)|m5hO%,ibE{F&s-g1Smr]+g@DvT^U=REt8r9bTFB.M8!%~2LJUOV[f+OxQ%C' );
define( 'LOGGED_IN_SALT',   'V^+8 UMLnjVP5POiSPNVKvc,[}5)Yc)7$72]X=DCoX/WgjT/@DlXDkh6M}.H#JYW' );
define( 'NONCE_SALT',       'WCFVUJq[M0OU^5AsDwD#:oH jPBwfIp8ku8PRg,cm4$n^fjvC*Ex1.3R3|x$6r,:' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
