import turtle

def star():
	turtle.TurtleScreen._RUNNING = True
	t = turtle.Turtle()
	screen=turtle.Screen()
	a = 0
	t.width(6)
	turtle.Screen().bgcolor("#000000")
	for a in range(0,7):
		t.pencolor("#de0b0b")
		t.fillcolor("#de0b0b")
		t.begin_fill()
		t.rt(26)
		t.pendown()
		t.fd(100)
		t.lt(51)
		t.fd(100)
		t.rt(51)
		t.bk(100)
		t.lt(51)
		t.bk(100)
		t.lt(26)
		t.end_fill()
		a = a + 1
#turtle.tracer(0, 0)
# main function
def main():
	screen = turtle.Screen()
	star()

	screen.exitonclick()

if __name__ == "__main__":
	main()
